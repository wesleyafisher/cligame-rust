use crate::gamemap;

pub struct Player {
    location: usize,
}

impl Player {
    pub fn get_player() -> Player {
        Player {
            location: usize::MIN,
        }
    }

    // &self short for self: &Self
    pub fn print_location(&self) {
        if self.location < 1 {
            println!("You are at base camp")
        } else {
            println!(
                "You are {} feet below",
                gamemap::get_cavern(self.location).depth
            )
        }
    }

    pub fn ascend(&mut self) {
        if self.location < 1 {
            println!("You are already at base camp!");
            return;
        }

        self.location -= 1;
        println!("You head back the way you Cave...");
        self.print_location();
    }

    pub fn descend(&mut self) {
        println!("You head deeper into the Cave...");

        if gamemap::has_next_cavern(self.location) {
            gamemap::generate_cavern()
        }
        self.location += 1;

        self.print_location()
    }
}
