use std::io;
use std::io::prelude::*;

mod gamemap;
mod gameplayer;

const GAME_NAME: &str = "cligame";

fn main() {
    print!("Loading player... ");
    let mut player = gameplayer::Player::get_player();
    println!("done");

    println!(
        "Welcome to {}. Type 'help' to get more information.",
        GAME_NAME
    );

    for line in io::stdin().lock().lines() {
        match line.unwrap().as_str() {
            "help" => {
                println!("help: display this message");
                println!("logout: stop playing {}", GAME_NAME);
                println!("location: display your current location");
                println!("ascend: head back the way you came");
                println!("descend: head deeper into the cave");
            }
            "logout" => return,
            "location" => player.print_location(),
            "ascend" => player.ascend(),
            "descend" => player.descend(),
            command => {
                println!("{} is not a valid command", command);
                println!("help: display this message");
                println!("logout: stop playing {}", GAME_NAME);
                println!("location: display your current location");
                println!("ascend: head back the way you came");
                println!("descend: head deeper into the cave");
            }
        }
    }
}
