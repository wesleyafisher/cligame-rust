static cave: Vec<Cavern> = vec![Cavern { depth: 0 }];

struct Cavern {
    pub depth: i32,
}

pub fn get_depth(index: usize) -> i32 {
    get_cavern(index).depth
}

pub fn get_cavern(index: usize) -> Cavern {
    cave[index]
}

pub fn has_next_cavern(index: usize) -> bool {
    index >= cave.len()
}

pub fn generate_cavern() {
    cave.push(Cavern {
        depth: 20 + cave[cave.len()].depth,
    })
}
