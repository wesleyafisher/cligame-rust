Install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

Uninstall rust
rustup self uninstall

Commands
cargo init <project name> - make a new project
cargo check - check if compiles
cargo test - run tests
cargo fmt - format files
cargo run - compile and run project
cargo build - compile project
cargo build --release - compile with optimizations
cargo clean - remove build artifacts

Changelog
Commit | Build time | Run time | Binary size | Comment
1      | ?          | ?        | ?           | Initialize project and print out welcome message
2      | 0m0.441s   | 0m0.001s | 3.2M        | Setup REPL with commands help and logout
